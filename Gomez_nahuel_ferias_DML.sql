--1. Obtenga el nombre y apellido de los usuarios que no están relacionados con ninguna feria ordenados por nombre.
SELECT nombre,apellido

FROM "user" U
WHERE U.id NOT IN (
	SELECT user_id
	FROM user_feria
)
ORDER BY nombre ASC;
--2. Obtenga el nombre y apellido de los usuarios que están relacionados con SOLAMENTE 1 feria ordenados por apellido.
SELECT nombre, apellido
FROM "user" U
WHERE U.id IN
	(SELECT user_id
	FROM (SELECT user_id, COUNT(*) AS apariciones
		FROM user_feria 
		GROUP BY user_id
		) as aparicionesUsuario
	WHERE aparicionesUsuario.apariciones =1)
ORDER BY apellido DESC;

--3. Obtenga el nombre y apellido de los usuarios que no están relacionados con más de una feria.
SELECT nombre, apellido
FROM "user" U
WHERE U.id IN
	(SELECT user_id
	FROM (SELECT user_id, COUNT(*) AS apariciones
		FROM user_feria 
		GROUP BY user_id
		) as aparicionesUsuario
	WHERE aparicionesUsuario.apariciones !=0);

--4.Obtenga el precio por kilo promediado por mes de cada producto, ordenados por tipo de producto ascendente, por especie y variedad del mismo y por precio por kilo descendente.
SElECT  prom,date_part,producto_declarado_id,especie,variedad,nombre
FROM producto_tipo JOIN 
	(SELECT prom,date_part,producto_declarado_id,especie,variedad,tipo_id
	FROM producto JOIN
		(SELECT producto_declarado_id,date_part('month',fecha),avg(precio_por_bulto/peso_por_bulto) AS prom
		FROM declaracion_individual
		GROUP BY producto_declarado_id, date_part('month',fecha)) AS promedio_kilo
	ON producto.id = promedio_kilo.producto_declarado_id) AS dato_sin_tipo
ON producto_tipo.id = dato_sin_tipo.tipo_id
ORDER BY nombre,especie,variedad ASC, prom DESC;

--5. Seleccione qué ferias están registradas pero no tienen ninguna declaración
SELECT  nombre,id
FROM feria
WHERE feria.id NOT IN (
	SELECT feria_id
	FROM declaracion);

--6. Seleccione el nombre, apellido y correo electrónico de los usuarios que hicieron declaraciones de ferias con las que no están relacionados.
SELECT DISTINCT user_autor_id,nombre,apellido,email 
FROM "user" join
	(SELECT *
	FROM declaracion JOIN user_feria
	ON user_feria.feria_id = declaracion.feria_id) AS decl
ON "user".id = user_autor_id
WHERE decl.user_id != decl.user_autor_id;

--7. Selecciones aquellas frutas cuyo precio promedio por kilo histórico no supere los 50 pesos.
SELECT especie
FROM producto JOIN 
	(SELECT producto_declarado_id, avg(precio_por_bulto/peso_por_bulto) AS prom
	FROM declaracion_individual
	GROUP BY producto_declarado_id) AS prom_hist
ON prom_hist.producto_declarado_id = producto.id
WHERE prom_hist.prom<50 and producto.tipo_id IN(
	SELECT id
	FROM producto_tipo
	WHERE nombre = 'Fruta'
	);
	
--8. Obtenga, ordenados alfabéticamente, el nombre y apellido de los usuarios que sólo frutas tienen en sus declaraciones (de acuerdo al tipo de producto).
SELECT nombre,apellido
FROM "user" JOIN
-- De este lado los que declararon solo fruta
(SELECT DISTINCT user_autor_id 
FROM declaracion
WHERE declaracion.id IN 
	(SELECT DISTINCT declaracion_id 
	FROM declaracion_individual
	WHERE declaracion_individual.producto_declarado_id IN
		(SELECT id
		FROM producto 
		WHERE producto.tipo_id in (
			Select id
			FROM producto_tipo
			WHERE nombre = 'Fruta'
			)
		 )
	 )
	 
	EXCEPT
	
--De este lado solo los que declararon productos que NO son fruta
SELECT DISTINCT user_autor_id 
FROM declaracion
WHERE declaracion.id IN 
	(SELECT DISTINCT declaracion_id 
	FROM declaracion_individual
	WHERE declaracion_individual.producto_declarado_id IN
		(SELECT DISTINCT id
		FROM producto 
		WHERE producto.tipo_id in (
			Select id
			FROM producto_tipo
			WHERE nombre != 'Fruta'
			)
		 )
	 )
) AS solo_Fruta

ON solo_fruta.user_autor_id = "user".id
ORDER BY nombre ASC;


--9. Obtenga un listado que muestre la cantidad de ferias por zona, ordenados descendentemente por cantidad.
SELECT COUNT(*),zona
FROM feria
GROUP BY zona
ORDER BY COUNT(*) DESC;

--10. Obtenga un listado que muestre la cantidad de ferias por zona, ordenados descendentemente (el listado debe excluir a las ferias sin declaraciones).
SELECT COUNT (*), zona
FROM feria
WHERE feria.id IN (
	SELECT feria_id
	FROM declaracion
	)
GROUP BY zona
ORDER BY COUNT(*) DESC;

--11. Obtenga un listado que muestre la cantidad de ferias por zona, ordenados descendentemente (el listado debe incluir a las ferias sin declaraciones).
SELECT COUNT(*),zona
FROM feria
GROUP BY zona
ORDER BY COUNT(*) DESC;

/*12. Obtenga un listado que muestre, de cada localidad donde haya usuarios registrados, el promedio de kilos  por bulto, el máximo de kilos por bulto y 
el mínimo de kilos por bulto de naranjas ofrecidos en ferias de ese distrito.*/

SELECT localidad,avg(peso_por_bulto),MAX (peso_por_bulto),MIN (peso_por_bulto)
FROM declaracion_individual JOIN

	(SELECT id,localidad,feria_id
	FROM declaracion JOIN
		(	SELECT localidad, id as idFeria
			FROM feria
			WHERE feria.id in (
				SELECT feria_id
				FROM user_feria
			)
		) AS loc_feria
	ON loc_feria.idFeria = declaracion.feria_id) AS feria_dec_loc
ON declaracion_individual.declaracion_id = feria_dec_loc.id
WHERE declaracion_individual.producto_declarado_id IN(
		SELECT id
		FROM producto
		WHERE producto.especie = 'NARANJA'
	)
	
GROUP BY localidad;

--13. En la tabla de productos conocemos su PK, pero es necesario impedir que pueda repetirse especie y variedad. Explique cómo lo haría e impleméntelo.
--Query puesta en el archivo de DDL 

--14. Cree una vista (view) con la información de correo del usuario, nombre, ubicación de todas las ferias con
--las que está relacionado. Dicho listado debe incluir a los usuarios que no tienen ferias asociadas
--Query puesta en el archivo de DDL 

--15.Obtenga un listado con el precio promedio, precios máximos y mínimo por producto en la semana actual.
SELECT avg(precio_por_bulto),min(precio_por_bulto),max(precio_por_bulto),producto_declarado_id
FROM declaracion_individual D
WHERE date_part('week',NOW()) = date_part('week',D.fecha) AND date_part('year',NOW()) = date_part('year',D.fecha)
GROUP BY producto_declarado_id

--16. Obtenga el precio promedio por producto y por zona en la semana anterior a la actual.
SELECT AVG(precio_por_bulto),zona,producto_declarado_id
FROM feria JOIN
	(SELECT producto_declarado_id,feria_id,fecha,precio_por_bulto
	FROM declaracion JOIN 
	declaracion_individual
	ON declaracion.id = declaracion_individual.declaracion_id) AS prod_fecha
	ON feria.id=prod_fecha.feria_id
	WHERE date_part('week',NOW())-1 = date_part('week',prod_fecha.fecha) AND date_part('year',NOW()) = date_part('year',prod_fecha.fecha)
	GROUP BY zona,producto_declarado_id
--17. Con el uso del sistema se identificaron muchísimas consultas buscando productos por su especie y variedad en la condición, cree un índice adecuado para dicha búsqueda.
--QUERY PUESTA EN ARCHIVO DDL

--18 Obtenga las 3 ferias con más usuarios que no hayan hecho declaraciones o que sólo las hayan hecho en ferias con menos de 50 puestos.
SELECT * FROM
	(SELECT feria_id,count(user_id) as usuarios
	FROM user_feria
	WHERE user_id NOT IN (
		SELECT user_autor_id
		FROM declaracion
		)
	GROUP BY feria_id




	UNION


SELECT feria_id,COUNT(user_id) as usuarios
FROM user_feria
WHERE user_feria.feria_id in
	(SELECT user_autor_id
	FROM declaracion
	WHERE declaracion.feria_id in
		(SELECT id 
		FROM feria
		WHERE cantidad_puestos<50)

			EXCEPT

	SELECT user_autor_id
	FROM declaracion
	WHERE declaracion.feria_id in
		(SELECT id 
		FROM feria
		WHERE cantidad_puestos>50)

	)
GROUP BY feria_id
) AS calc

ORDER BY usuarios DESC
FETCH FIRST 3 ROWS ONLY